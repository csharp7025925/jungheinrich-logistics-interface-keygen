private static string getLicenceKey()
{
    string machineId = "50DBCA6437BF0EE2"; // Unique Id for each device
    string salt = "6F6A02550D0259C1A0D656DBF8F03707745F18FF";
    byte[] bytes = Encoding.Default.GetBytes(machineId + salt);
    byte[] hashBytes = System.Security.Cryptography.SHA1.HashData(bytes);
    StringBuilder stringBuilder = new();
    for (int i = 0; i < hashBytes.Length; i++)
    {
        stringBuilder.Append(hashBytes[i].ToString("X2"));
    }
        
    return stringBuilder.ToString()[..16];
}
