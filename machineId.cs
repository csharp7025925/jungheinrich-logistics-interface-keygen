private static string getMachineId()
{
    if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
    {
        return string.Empty;
    }

    [DllImport("kernel32.dll")]
    static extern long GetVolumeInformation(
        string A_0,
        StringBuilder A_1,
        uint A_2,
        ref uint A_3,
        ref uint A_4,
        ref uint A_5,
        StringBuilder A_6,
        uint A_7);

    uint A_4 = 0;
    uint A_5 = 0;
    StringBuilder A_1 = new(256);
    StringBuilder A_6 = new(256);

    string machineId = string.Empty;
    uint volumeSerialNumber = 0;
    string JHRandom;
    
    GetVolumeInformation(@"c:\", A_1, (uint)A_1.Capacity, ref volumeSerialNumber, ref A_4, ref A_5, A_6, (uint)A_6.Capacity);
    machineId += volumeSerialNumber.ToString();
    JHRandom = Registry.CurrentUser.GetValue(@"Software\SysServices\ManufacturerId", "").ToString().Trim();
    if (JHRandom == string.Empty)
    {
        JHRandom = new Random().Next(12000, 19000).ToString();
        Registry.CurrentUser.SetValue(@"Software\SysServices\ManufacturerId", JHRandom);
    }
    return machineId += JHRandom;
}
